<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# ambtec project

ambtec projects are made to rethink current approaches, test new ways to solve challenges and to make development creation easier.

- *LEO Service Framework*: This project features a low code and event driven plattform to orchestrate process flows and with that to function as a framework for easier service development and maintenance.

## Project page

The ambtec project page works as the main hub to inform, showcase and provide services of the project.

### Setup

Run the following command to install

```
npm i
```

Then run the project page on port 8000

```
npm start
```

Build the project via

```
npm run build
```

## Widget support

Dynamic services are add to the static page with the use of web components, so called 'Widgets'. Widgets are standalone solutions. They are served from different repositories and life within the provided context of the host while beeing completely seperated at the same time. Beside specific defined communication channels there is no further communication possible between a Widget and a host.

### Dashboard Widget

Coummincation flow is Project Page to Dashboard Widget only \
![Project Page - Dashboard Widget Coummincation flow](./assets/Project-Page.drawio.png "Project Page - Dashboard Widget Coummincation flow") \
(Made with https://app.diagrams.net/)

- 1: The project page deliver a context in within the Dashboard Widget lifes. On startup a predefinied parameter transfer take place. See [Dashboard Widget](https://gitlab.com/ambtec/dashboard-widget) for configuration options.

## License
MIT

## Credits

- Thanks to [Tailwind CSS and their utility first framework](https://tailwindcss.com/docs/installation)
- Thanks to [Always-Free Docs Template](https://github.com/always-free/always-free-docs-template)
