// const path = require("path");

function getCloudSidebar() {
  return [
    {
      title: "Cloud",
      collapsable: false,
      children: ["", "upstash", "redislabs"],
    },
  ];
}

function getServerSidebar() {
  return [
    {
      title: "Servers",
      collapsable: false,
      children: [""],
    },
    {
      title: "Database",
      collapsable: false,
      children: ["database/", "database/postgres"],
    },
    {
      title: "Cache",
      collapsable: false,
      children: ["cache/", "cache/redis"],
    },
  ];
}

function googleAnalytics(measurementId) {
  return process.env.NODE_ENV === "development"
    ? []
    : [
        //         [
        //           "script",
        //           {
        //             async: true,
        //             src: `https://www.googletagmanager.com/gtag/js?id=${measurementId}`,
        //           },
        //         ],
        //         [
        //           "script",
        //           {},
        //           `
        // window.dataLayer = window.dataLayer || [];
        // function gtag(){dataLayer.push(arguments);}
        // gtag('js', new Date());
        // gtag('config', '${measurementId}', { 'anonymize_ip': true });
        // router.afterEach(function (to) {
        // gtag('set', 'page', router.app.$withBase(to.fullPath))
        // gtag('send', 'pageview')
        // })
        // `,
        //         ],
      ];
}

function widgets() {
  const timestamp = Math.floor(Date.now() / 1000);
  const scripts = [
    [
      "script",
      {
        src: `http://dashboard-widget.localhost/js/chunk-vendors.${timestamp}.js`,
      },
    ],
    [
      "script",
      {
        src: `http://dashboard-widget.localhost/js/app.${timestamp}.js`,
      },
    ],
    [
      "script",
      {
        src: `http://iam-widget.localhost/js/chunk-vendors.${timestamp}.js`,
      },
    ],
    [
      "script",
      {
        src: `http://iam-widget.localhost/js/app.${timestamp}.js`,
      },
    ],
  ];
  return scripts;
}

module.exports = {
  title: "ambtec",
  description:
    "Goodness of open source stack technologies and probable free hosting solution",
  head: [
    [
      "link",
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css",
      },
    ],
    ...googleAnalytics("G-Z07GXXXXXX"),
    ...widgets(),
  ],
  themeConfig: {
    lastUpdated: "Last Updated",
    activeHeaderLinks: true,
    nextLinks: true,
    prevLinks: true,
    smoothScroll: true,
    displayAllHeaders: true,
    search: true,
    searchMaxSuggestions: 5,
    nav: [
      { text: "Apps", link: "/apps/" },
      { text: "Servers", link: "/servers/" },
      { text: "Cloud", link: "/cloud/" },
      { text: "Blogs", link: "/blogs/" },
      { text: "Dashboard", link: "/dashboard/" },
    ],
    sidebar: {
      "/apps/": "auto",
      "/servers/": getServerSidebar(),
      "/cloud/": getCloudSidebar(),
      "/blogs/": "auto",
      "/": [""],
    },
    sidebarDepth: 2,
  },
  markdown: {
    lineNumbers: true,
    toc: { includeLevel: [1, 2] },
  },
  plugins: [
    ["@vuepress/back-to-top", true],
    ["vuepress-plugin-code-copy", true],
  ],
  globalUIComponents: [
    // 'Component-1',
    "Iam",
  ],
};
