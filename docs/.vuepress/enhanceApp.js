import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
// import postRobot from "post-robot";

export default ({
  Vue, // the version of Vue being used in the VuePress app
  options, // the options for the root Vue instance
  router, // the router instance for the app
  siteData, // site metadata
  isServer, // The current application configuration is in server rendering or client
}) => {
  Vue.use(Vuetify);
  // Vue.use(postRobot);

  // if (!isServer){
  //   import('post-robot').then(module => {
  //       const postRobot = module
  //       Vue.use(postRobot);
  //   })
  // }

  Vue.config.ignoredElements = ["dashboard-widget", "iam-widget"];
};
